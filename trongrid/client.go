package trongrid

import (
	"errors"
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type Client struct {
	httpClient *http.Client
	apiKey     string
}

type GetWalletBalanceRequest struct {
	WalletAddress string
}

type GetWalletTransactionRequestQuery struct {
	Limit        int32 `schema:"limit"`
	MitTimestamp int64 `schema:"min_timestamp"`
}

type GetWalletTransactionRequest struct {
	WalletAddress string
	Query         GetWalletTransactionRequestQuery
}

type GetWalletBalanceResponse struct {
	Balance int64 `json:"balance"`
}

type TransactionContract struct {
	Type      string `json:"type"`
	Parameter struct {
		Value struct {
			Data         *string `json:"data"`
			Amount       *uint64 `json:"amount"`
			OwnerAddress string  `json:"owner_address"`
			ToAddress    *string `json:"to_address"`
		} `json:"value"`
	} `json:"parameter"`
}

type Transaction struct {
	TxID string `json:"txID"`
	Fee  uint64 `json:"net_fee"`
	Ret  []struct {
		ContractRet string `json:"contractRet"`
	}
	RawDataHex string `json:"raw_data_hex"`
	RawData    struct {
		Contract  []TransactionContract `json:"contract"`
		Timestamp int64                 `json:"timestamp"`
	} `json:"raw_data"`
}

type GetWalletTransactionsResponse struct {
	Success bool          `json:"success"`
	Data    []Transaction `json:"data"`
}

type GetBlockTransactionsRequest struct {
	Number uint64 `schema:"num"`
}

type GetBlockTransactionsResponse struct {
	Transactions []Transaction `json:"transactions"`
}

func NewClient(httpClient *http.Client, apiKey string) Client {
	return Client{
		httpClient: httpClient,
		apiKey:     apiKey,
	}
}

func (c Client) GetWalletBalance(request GetWalletBalanceRequest) (*GetWalletBalanceResponse, error) {
	var response GetWalletBalanceResponse

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: nil,
		Data: http.RequestData{
			Path:    fmt.Sprintf("/v1/accounts/%s", request.WalletAddress),
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying to get wallet balance in tongrid: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying to get wallet balance in tongrid: %s", httpError)
	}

	return &response, nil
}

func (c Client) GetBlockTransactions(request GetBlockTransactionsRequest) (*GetBlockTransactionsResponse, error) {
	var response GetBlockTransactionsResponse

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/wallet/getblockbynum",
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying to get transactions in tongrid: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying to get transactions in tongrid: %s", httpError)
	}

	return &response, nil
}

func (c Client) GetWalletTransactions(request GetWalletTransactionRequest) (*GetWalletTransactionsResponse, error) {
	var response GetWalletTransactionsResponse

	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request.Query,
		Data: http.RequestData{
			Path:    fmt.Sprintf("/v1/accounts/%s/transactions", request.WalletAddress),
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while trying to get wallet transactions in tongrid: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while trying to get wallet transactions in tongrid: %s", httpError)
	}

	if !response.Success {
		return nil, errors.New("response is not successful, can not get transactions by wallet in tongrid")
	}

	return &response, nil
}
